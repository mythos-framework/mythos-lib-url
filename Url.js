"use strict";

var url = require('url');

var Url = function (options) {
	this.options = options;
};

Url.prototype.base = function(path) {
	return this.options.base_url + path;
};

Url.prototype.asset = function(path) {
	return this.options.assets_url + path;
};

Url.prototype.bower = function(path) {
	return this.options.bower_url + path;
};

module.exports = Url;